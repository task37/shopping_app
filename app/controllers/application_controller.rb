class ApplicationController < ActionController::Base
  # Adding flash types
  add_flash_types :warning,:danger, :success, :info

  # Check whether user logged in or not
  def is_user_logged_in?
    if session[:user_id].nil?
      false
    else
      true
    end
  end

  # set session user_id
  def set_session_user_id user_id 
    session[:user_id] = user_id 
  end 

  # get session user_id
  def get_session_user_id
    session[:user_id]
  end 

  # Get current user name
  def current_user
    begin
      user = User.find(get_session_user_id)
      user.email.split('@').first
    rescue => e
      set_session_user_id nil
    end
  end

  helper_method :is_user_logged_in?, :current_user
end
