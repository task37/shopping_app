class ProductController < ApplicationController
  #before_action :authorize_user, only:[:show_cart,:my_orders,:add_cart]

  def add_cart
    product_ids = params[:product_ids]
    # selected checkbox product ids
    product_ids = product_ids.select {|key,value| value == "1"}.keys 
    product_ids.each do |product_id|
      @status = CartItem.create(user_id:get_session_user_id,product_id:product_id)
    end
    @products = Product.all
    render "/product/list", products:@products, locals: { notice:'Item(s) added to cart' }
  end

  def list
    # Get all my products
    @products = Product.all
  end

  def show_cart
    @cart_items = CartItem.where(user_id:get_session_user_id)
    respond_to do |format|
      format.html {render layout:false}
    end
  end
  
  def items_checkout
    cart_ids = params[:cart_ids].select {|key,value| value == "1"}.keys
    quantities = params[:quantities]
    # Update selected quantities to cart item
    cart_ids.each do |cart_id|
      CartItem.update(cart_id,quantity:quantities[cart_id])
    end
    # Fetch records for items checkout
    @checkout = CartItem.where(id: cart_ids)
  end


  def place_order
    # Get params
    ids = params[:cart_ids].split(',')
    price = params[:price]

    # Create order record
    @order = OrderDetail.create(user_id:get_session_user_id,amount:price,status:'Completed')

    if @order.valid?
      @carts_items = CartItem.where(id: ids)
      @carts_items.each do |item|
        @order_item = OrderItem.create(user_id:item.user_id,product_id:item.product_id,quantity:item.quantity,order_detail_id:@order.id)
        # Delete cart items
        item.destroy if @order_item.valid?
      end
      # Check pending cart items in background and send email notification to user
      BaseWorkerJob.perform_async(session[:user_id])
    end
    
  end

  def make_payment
    ids = params[:cart_ids].split(',')
    @order_items = CartItem.where(id: ids)
  end

  def my_orders
    user_id = session[:user_id]
    user = User.find(user_id)
    @my_orders = user.order_details.order("id desc")
    respond_to do |format|
      format.html {render layout:false}
    end
  end

  private
  def authorize_user 
    unless is_user_logged_in?
      redirect_to "/users", :alert => "Please Login First"
    end
  end

end
