class UsersController < ApplicationController
  def login
    # Get user params
    user_params = get_params

    # Check existence of user
    @user = User.find_by_email(user_params[:email])

    # User validation 
    if @user and @user.valid_password?(user_params[:password])
      set_session_user_id @user.id 
      redirect_to "/product/list"
      
    elsif @user.nil?  # create non existent user
      @user = User.create(email:user_params[:email],password:user_params[:password])

      if @user.valid?
        set_session_user_id @user.id 
        redirect_to "/product/list"
      else
        redirect_to "/users", :alert => "Login failed:#{@user.errors.full_messages.join(',')}"
      end

    else
      redirect_to '/users', :alert => "Login failed - Invalid credentials"
    end   
  end

  def logout
    # clear session 
    set_session_user_id nil
    redirect_to "/users"
  end

  private
  # Take permitted params
  def get_params
    params.require(:user).permit(:email,:password)
  end

end
