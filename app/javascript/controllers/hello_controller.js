import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
	connect() {
		//('Stimulus connected');
	}

	mycart() {
		console.log('my cart clicked');
		const csrfToken = document.querySelector("[name='csrf-token']").content
		let content=document.querySelector('div#content');

		fetch(`/product/show_cart`, {
			method: 'GET', // *GET, POST, PUT, DELETE, etc.
			mode: 'cors', // no-cors, *cors, same-origin
			cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
			credentials: 'same-origin', // include, *same-origin, omit
			headers: {
				'Content-Type': 'text/html',
				'X-CSRF-Token': csrfToken
			},
		})
			.then(response => {
				return response.text();  // has promise object with result
			})
			.then(html=> {
				console.log(html);
				content.innerHTML=html
			}).catch(err => {
				console.log('error : ',err);
			})
	}
	myorder() {
		console.log('my order clicked');
		const csrfToken = document.querySelector("[name='csrf-token']").content
		let content=document.querySelector('div#content');

		fetch(`/product/my_orders`, {
			method: 'GET', // *GET, POST, PUT, DELETE, etc.
			mode: 'cors', // no-cors, *cors, same-origin
			cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
			credentials: 'same-origin', // include, *same-origin, omit
			headers: {
				'Content-Type': 'text/html',
				'X-CSRF-Token': csrfToken
			},
		})
			.then(response => {
				return response.text();  // has promise object with result
			})
			.then(html=> {
				console.log(html);
				content.innerHTML=html
			}).catch(err => {
				console.log('error : ',err);
			})
	}
}
