class ApplicationMailer < ActionMailer::Base
  default from: "noreply@shopeasy.com"
  layout "mailer"
end
