class PendingCartItemsMailer < ApplicationMailer
  default :from => 'noreply@shopeasy.com'

  # send a pending cart to the user, pass in the user object that  contains the user's email address
  def send_carts_email
    @user = params[:user]
    @left_cart_items = params[:cart_items]
    mail( :to => @user.email,
    #mail( :to => 'vinothini182@gmail.com',
    :subject => 'Hey! You Left Some Cart Items' )
  end
end

