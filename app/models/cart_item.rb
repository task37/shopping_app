class CartItem < ApplicationRecord
  validates_presence_of :user_id, :product_id, :quantity
  validates_numericality_of :quantity, greater_than:0

  belongs_to :product
  belongs_to :user
end
