class OrderDetail < ApplicationRecord
  validates_presence_of :user_id, :amount, :status
  validates_numericality_of :amount, greater_than:0
  belongs_to :user
  has_many :order_items
  has_many :products, through: :order_items
end
