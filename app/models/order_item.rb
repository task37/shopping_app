class OrderItem < ApplicationRecord
  validates_presence_of :order_detail_id, :product_id, :user_id, :quantity
  validates_numericality_of :quantity, greater_than:0
  belongs_to :order_detail
  belongs_to :product
  belongs_to :user 
end
