class Product < ApplicationRecord
  # Validations
  validates :product_name, uniqueness:true, presence:true
  validates :price, presence:true
  validates :description, presence:true, length: { minimum:10 }
  validates_numericality_of :price, greater_than:0

  has_many :cart_items
  has_many :order_items
  has_many :order_details, through: :order_items
end
