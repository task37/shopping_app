class BaseWorkerJob
  include Sidekiq::Job
  #queue_as :default

  def perform(user_id)
    #sleep 13
    user = User.find(user_id)
    @carts = CartItem.where(user_id: user_id)

    left_cart_items = []
    @carts.each do |item|
      left_cart_items.push(item.product.product_name)
    end

    # Will display current time, milliseconds included
    if left_cart_items.size > 0
      p "#{user.email}... Left cart items : #{left_cart_items.join(',')} - #{Time.now().strftime('%F - %H:%M:%S.%L')}"
      # Send mails 
      PendingCartItemsMailer.with(user: user,cart_items:left_cart_items).send_carts_email.deliver_now
    else
      p "#{user.email}... No cart items - #{Time.now().strftime('%F - %H:%M:%S.%L')}"
    end
  end
end
