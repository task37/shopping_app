Rails.application.routes.draw do

  get 'users/login'
  post 'users/login'
  get 'users/logout'

  get 'product/add'
  post 'product/add'
  get 'product/add_cart'
  post 'product/add_cart'

  get 'product/show_cart'
  get 'product/my_cart'

  #get "products", to: "product/list"
  get '/product/list' => 'product#list', as: 'products'
  get 'product/list'
  get 'product/my_orders'
  post 'product/my_orders'

  get 'product/place_order'
  post 'product/place_order'

  get 'product/items_checkout'
  post 'product/items_checkout'

  get 'product/make_payment'
  post 'product/make_payment'

  get 'product/turbo_submit'

  require 'sidekiq/web'
  Rails.application.routes.draw do
  get 'others/job_done'
    mount Sidekiq::Web => '/sidekiq'
  end

  devise_scope :user do
    # Redirests signing out users back to sign-in
    get "users", to: "devise/sessions#new"
    root to: "devise/sessions#new"
  end
  #root "product#list"
  devise_for :users


  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
