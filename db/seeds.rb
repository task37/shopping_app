# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
Product.create([{product_name:'Bluetooth Headphone',description:'Wireless-Bluetooth Headphone',price:450},{product_name:'32 Inch Sony Television',description:'32 inch Sony Television',price:30000},{product_name:'Fastrack Watch',description:'Fastrack Watch in Silver Color',price:2000},{product_name:'Ceiling Fan',description:'Ceiling fan in multiple colors',price:1500},{product_name:'Iphone Mobile',description:'Iphone mobile',price:50000},{product_name:'Moto G6 Mobile Back Cover',description:'Mobile Back Cover',price:200}])

