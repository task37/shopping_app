require 'rails_helper'

RSpec.describe CartItem, type: :model do

  before {
    @user = User.new(email:Faker::Internet.email,password:'testpassword')
    @user.save
    @product = Product.new(product_name:Faker::Appliance,description:'Electronic Appliance',price:5000)
    @product.save
    @cart_item = CartItem.new(user_id:@user.id, product_id:@product.id,quantity:1)
    @cart_item.save
  }

  it 'it is invalid without user id' do
    @cart_item.user_id = nil
    expect(@cart_item).not_to be_valid
  end

  it 'it is invalid without product id' do
    @cart_item.product_id = nil
    expect(@cart_item).not_to be_valid
  end

  it 'it is invalid without quantity' do
    @cart_item.quantity = nil
    expect(@cart_item).not_to be_valid
  end

  it 'Minimum quantity should be 1' do
    @cart_item.quantity = 0
    expect(@cart_item).not_to be_valid
  end

  #it 'invalid if product id is improper' do
  #  @cart_item.product_id = 12
  #  expect(@cart_item).not_to be_valid
  #end

end
