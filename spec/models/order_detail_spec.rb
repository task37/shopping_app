require 'rails_helper'

RSpec.describe OrderDetail, type: :model do

  before {
    @user = User.new(email:Faker::Internet.email,password:'testpassword')
    @user.save
    @order_detail = OrderDetail.new(user_id:@user.id,amount:10000,status:'completed')
    @order_detail.save
  }

  it 'It should have user id' do
    @order_detail.user_id = nil
    expect(@order_detail).not_to be_valid
  end

  it 'It should have amount value' do
    @order_detail.amount = nil
    expect(@order_detail).not_to be_valid
  end

  it 'It should have status info' do
    @order_detail.status = nil
    expect(@order_detail).not_to be_valid
  end

  it 'Minimum amount should be 1' do
    @order_detail.amount = 0
    expect(@order_detail).not_to be_valid
  end

end
