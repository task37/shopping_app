require 'rails_helper'

RSpec.describe OrderItem, type: :model do
  before {
    @user = User.new(email:Faker::Internet.email,password:'testpassword')
    @user.save
    @product = Product.new(product_name:Faker::Appliance,description:'Electronic Appliance',price:5000)
    @product.save
    @order_detail = OrderDetail.new(user_id:@user.id,amount:10000,status:'completed')
    @order_detail.save
    @order_item = OrderItem.new(user_id:@user.id, product_id:@product.id, quantity:2, order_detail_id:@order_detail.id)
    @order_item.save
  }

  it 'It should have user id' do
    @order_item.user_id = nil
    expect(@order_item).not_to be_valid
  end

  it 'It should have product id' do
    @order_item.product_id = nil
    expect(@order_item).not_to be_valid
  end

  it 'It should have order detail id' do
    @order_item.order_detail_id = nil
    expect(@order_item).not_to be_valid
  end

  it 'Minimum quantity should be 1' do
    @order_item.quantity = 0
    expect(@order_item).not_to be_valid
  end

end
