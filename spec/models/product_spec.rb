require 'rails_helper'

RSpec.describe Product, type: :model do

  subject { Product.create(product_name:Faker::Appliance.equipment,description:'Electronic Appliance',price:1000) }
  before { subject.save }

  it "it is invalid without name of product" do
    product = Product.new
    expect(product).not_to be_valid
  end

  it "it is invalid without description" do
    subject.description = nil
    expect(subject).not_to be_valid
  end

  it "it is invalid without product price" do
    subject.price = nil
    expect(subject).not_to be_valid
  end

  it "description length should not lesser than 10" do
    subject.description = "a"
    expect(subject).not_to be_valid
  end

  it "price should not lesser than 1" do
    subject.price = 0
    expect(subject).not_to be_valid
  end

end
