require 'rails_helper'

RSpec.describe User, type: :model do
  subject { User.new(email:Faker::Internet.email,password:'testpassword') }
  before { subject.save }
  it "is not valid without email and password" do
    expect(User.new).to_not be_valid
  end

  it "is not valid without email" do
    subject.email = nil
    expect(subject).to_not be_valid
  end

  it "is not valid without password" do
    #subject.password = nil
    user = User.new(email:Faker::Internet.email)
    expect(user).to_not be_valid
  end

  it "email should be in proper format" do
    subject.email = "test"
    expect(subject).to_not be_valid
  end

end
